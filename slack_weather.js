/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ______     ______     ______   __  __     __     ______
 /\  == \   /\  __ \   /\__  _\ /\ \/ /    /\ \   /\__  _\
 \ \  __<   \ \ \/\ \  \/_/\ \/ \ \  _"-.  \ \ \  \/_/\ \/
 \ \_____\  \ \_____\    \ \_\  \ \_\ \_\  \ \_\    \ \_\
 \/_____/   \/_____/     \/_/   \/_/\/_/   \/_/     \/_/


 This is a sample Slack bot built with Botkit.

 This bot demonstrates many of the core features of Botkit:

 * Connect to Slack using the real time API
 * Receive messages based on "spoken" patterns
 * Reply to messages
 * Use the conversation system to ask questions
 * Use the built in storage system to store and retrieve information
 for a user.

 # RUN THE BOT:

 Get a Bot token from Slack:

 -> http://my.slack.com/services/new/bot

 Run your bot from the command line:

 token=<MY TOKEN> node slack_bot.js

 # USE THE BOT:

 Find your bot inside Slack to send it a direct message.

 Say: "Hello"

 The bot will reply "Hello!"

 Say: "who are you?"

 The bot will tell you its name, where it running, and for how long.

 Say: "Call me <nickname>"

 Tell the bot your nickname. Now you are friends.

 Say: "who am I?"

 The bot will tell you your nickname, if it knows one for you.

 Say: "shutdown"

 The bot will ask if you are sure, and then shut itself down.

 Make sure to invite your bot into other channels using /invite @<my bot>!

 # EXTEND THE BOT:

 Botkit has many features for building cool and useful bots!

 Read all about it here:

 -> http://howdy.ai/botkit

 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


if (!process.env.token) {
    console.log('Error: Specify token in environment');
    process.exit(1);
}

var Botkit = require('./lib/Botkit.js');
var os = require('os');
var _ = require('lodash');
var request = require('request');

var controller = Botkit.slackbot({
    // debug: true,
    json_file_store: './storage/json/',
});

var bot = controller.spawn({
    token: process.env.token
}).startRTM();

controller.hears(['天気のエリア情報を(取得|更新)'], 'direct_message,direct_mention,mention', function (bot, message) {
    var parser = require('xml2json'),
      xml_url = 'http://weather.livedoor.com/forecast/rss/primary_area.xml',
      json = ''
      ;

    request(xml_url, function(err, response, body) {
        if (err || response.statusCode !== 200) {
            console.log(response.statusCode + ': ' + body);
        } else {
            json = parser.toJson(body);
            json = JSON.parse(json);

            var prefectures = json.rss.channel[ 'ldWeather:source' ].pref;
            var cities = prefectures.map(function(pref){
                if (_.isArray(pref.city)) {
                    for (var i = 0; i < pref.city.length; i++) {
                        return pref.city[i];
                    }
                } else {
                    return pref.city;
                }
            });

            controller.storage.teams.save({
                id: 'area_point',
                date: _.now(),
                data: cities
            }, function(err, id) {
                // bot.reply(message, 'Got it. I will call you from now on.');
            });
        }
    });

    bot.reply(message, '天気の地域情報を' + message.match[1] + 'しました。');
});

controller.hears(['天気のエリアを教えて'], 'direct_message,direct_mention,mention', function (bot, message) {
    var area,
      msg
      ;
    controller.storage.teams.get('area_point', function (err, team) {
        area = team.data.map(function (city) {
            return city.title;
        });

        msg = {
            attachments : [
                {
                    "color" : "#764FA5",
                    "title" : 'お天気 エリア一覧',
                    "text" : area.join('、'),
                }
            ]
        }

        bot.reply(message, msg);

    })
})

controller.hears(['(.*)の天気'], 'direct_message,direct_mention,mention', function ( bot, message ) {
    var
      sprintf = require('sprintf-js').sprintf,
      request = require('request'),
      matches = message.text.match(/(.*)の天気/i),
      input_title = matches[1],
      api_uri = 'http://weather.livedoor.com/forecast/webservice/json/v1?city=',
      json = '',
      msg,
      obj,
      forecast
      ;

    // 地名からcity.idを取得する
    controller.storage.teams.get('area_point', function ( err, team ) {
        obj = team.data.map(function (city) {
            if (city.title === input_title) {
                return city.id;
            }
            return false
        }).filter(function(item) {
            return item !== false;
        });

        if (obj.length < 1) {
            bot.reply(message, input_title + '以外のキーワードで検索してください。ex. 那覇の天気');
        } else {
            // Weather Hack APIから天気情報を取得する
            request(api_uri + obj[0], function ( err, response, body ) {
                if (err || response.statusCode !== 200) {
                    console.error(response.statusCode + ': ' + body);
                } else {
                    json = JSON.parse(body);

                    forecast = json.forecasts[0];
                    var msg_text = sprintf("%sの%sの天気は、%sです。\n%s",
                      json.location.city,
                      forecast.dateLabel,
                      forecast.telop,
                      json.description.text
                    );
                    msg = {
                        attachments : [
                            {
                                "fallback" : "天気情報",
                                "color" : "#36a64f",
                                // "pretext" : json.title,
                                "author_name" : json.copyright.image.title,
                                "author_link" : json.copyright.image.link,
                                "author_icon" : json.copyright.image.url,
                                "title" : json.title,
                                "title_link" : json.link,
                                "text" : msg_text,
                                // "image_url" : forecast.image.url,
                                "thumb_url" : forecast.image.url
                            }
                        ]
                    }

                    bot.reply(message, msg);
                }
            });
        }
    });
});

controller.hears(['hello', 'hi'], 'direct_message,direct_mention,mention', function(bot, message) {

    bot.api.reactions.add({
        timestamp: message.ts,
        channel: message.channel,
        name: 'robot_face',
    }, function(err, res) {
        if (err) {
            bot.botkit.log('Failed to add emoji reaction :(', err);
        }
    });


    controller.storage.users.get(message.user, function(err, user) {
        if (user && user.name) {
            bot.reply(message, 'Hello ' + user.name + '!!');
        } else {
            bot.reply(message, 'Hello.');
        }
    });
});

controller.hears(['call me (.*)', 'my name is (.*)'], 'direct_message,direct_mention,mention', function(bot, message) {
    var name = message.match[1];
    controller.storage.users.get(message.user, function(err, user) {
        if (!user) {
            user = {
                id: message.user,
            };
        }
        user.name = name;
        controller.storage.users.save(user, function(err, id) {
            bot.reply(message, 'Got it. I will call you ' + user.name + ' from now on.');
        });
    });
});

controller.hears(['what is my name', 'who am i'], 'direct_message,direct_mention,mention', function(bot, message) {

    controller.storage.users.get(message.user, function(err, user) {
        if (user && user.name) {
            bot.reply(message, 'Your name is ' + user.name);
        } else {
            bot.startConversation(message, function(err, convo) {
                if (!err) {
                    convo.say('I do not know your name yet!');
                    convo.ask('What should I call you?', function(response, convo) {
                        convo.ask('You want me to call you `' + response.text + '`?', [
                            {
                                pattern: 'yes',
                                callback: function(response, convo) {
                                    // since no further messages are queued after this,
                                    // the conversation will end naturally with status == 'completed'
                                    convo.next();
                                }
                            },
                            {
                                pattern: 'no',
                                callback: function(response, convo) {
                                    // stop the conversation. this will cause it to end with status == 'stopped'
                                    convo.stop();
                                }
                            },
                            {
                                default: true,
                                callback: function(response, convo) {
                                    convo.repeat();
                                    convo.next();
                                }
                            }
                        ]);

                        convo.next();

                    }, {'key': 'nickname'}); // store the results in a field called nickname

                    convo.on('end', function(convo) {
                        if (convo.status == 'completed') {
                            bot.reply(message, 'OK! I will update my dossier...');

                            controller.storage.users.get(message.user, function(err, user) {
                                if (!user) {
                                    user = {
                                        id: message.user,
                                    };
                                }
                                user.name = convo.extractResponse('nickname');
                                controller.storage.users.save(user, function(err, id) {
                                    bot.reply(message, 'Got it. I will call you ' + user.name + ' from now on.');
                                });
                            });



                        } else {
                            // this happens if the conversation ended prematurely for some reason
                            bot.reply(message, 'OK, nevermind!');
                        }
                    });
                }
            });
        }
    });
});


controller.hears(['shutdown'], 'direct_message,direct_mention,mention', function(bot, message) {

    bot.startConversation(message, function(err, convo) {

        convo.ask('Are you sure you want me to shutdown?', [
            {
                pattern: bot.utterances.yes,
                callback: function(response, convo) {
                    convo.say('Bye!');
                    convo.next();
                    setTimeout(function() {
                        process.exit();
                    }, 3000);
                }
            },
            {
                pattern: bot.utterances.no,
                default: true,
                callback: function(response, convo) {
                    convo.say('*Phew!*');
                    convo.next();
                }
            }
        ]);
    });
});


controller.hears(['uptime', 'identify yourself', 'who are you', 'what is your name'],
  'direct_message,direct_mention,mention', function(bot, message) {

      var hostname = os.hostname();
      var uptime = formatUptime(process.uptime());

      bot.reply(message,
        ':robot_face: I am a bot named <@' + bot.identity.name +
        '>. I have been running for ' + uptime + ' on ' + hostname + '.');

  });

function formatUptime(uptime) {
    var unit = 'second';
    if (uptime > 60) {
        uptime = uptime / 60;
        unit = 'minute';
    }
    if (uptime > 60) {
        uptime = uptime / 60;
        unit = 'hour';
    }
    if (uptime != 1) {
        unit = unit + 's';
    }

    uptime = uptime + ' ' + unit;
    return uptime;
}
